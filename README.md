# vich_hashing

An implementation of a hash table based on VICH (variable insert coalesced hashing).

During writing a blog post about about unordered associative containers I investigated hash tables and how they are implemented. At that time I knew about using linked lists for resolving collisions, however I never heard about about coalesced hashing before. In order to understand it I decided to write a hash table using coalesced hashing.

To learn about VICH and to implement it I used the following two papers:

 * 'Implementations for Coalesced Hashing', by Jeffrey Scott Vitter
 * 'Deletion Algorithms for Coalesced Hashing', by W. C. CHEN and

Note: This implementation is for demonstration purposes only and is not meant to be used in real life applications. If you do so you do it on your own risk. I did lots of testing (there is a separate project with unit tests), still I never used it in production and I am sure there are lots of possibilities for improvements, especially performance wise.
